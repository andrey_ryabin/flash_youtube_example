package {

import flash.display.Loader;
import flash.display.LoaderInfo;
import flash.display.Sprite;

import flash.display.Stage;
import flash.display.StageAlign;
import flash.display.StageScaleMode;
import flash.events.Event;
import flash.net.URLRequest;
import flash.system.ApplicationDomain;
import flash.system.LoaderContext;
import flash.system.Security;

public class Flash_youtube extends Sprite {

    protected function getVar(name:String, defVal:String = ''):String{
        var val:String = this.loaderInfo.parameters[name];
        return (val !== undefined && val !== null) ? val : defVal;
    }

    public function Flash_youtube():void {
        //width = 550;
        //height = 375;

        stage.scaleMode = StageScaleMode.NO_SCALE;
        stage.align = StageAlign.TOP_LEFT;

        //stage.scaleMode = StageScaleMode.EXACT_FIT;
        //stage.align = StageAlign.TOP_LEFT;

        var video_code:String = getVar("video_code", "H9kJ75uY4vg");
        var video_width:String = getVar("video_width", '560');
        var video_height:String = getVar("video_height", '315');

        var video_url:String = "http://www.youtube.com/v/" + video_code + "?version=3";


        Security.allowDomain("www.youtube.com");

        var player:Object;

        //var context:LoaderContext = new LoaderContext();
        //context.checkPolicyFile = false;
        //context.securityDomain = SecurityDomain.currentDomain;
        //context.applicationDomain = ApplicationDomain.currentDomain;
        //var urlReq:URLRequest = new URLRequest("http://www.youtube.com/v/FYaHw7402OQ?version=3");

        var loader:Loader = new Loader();
        loader.contentLoaderInfo.addEventListener(Event.INIT, onLoaderInit);
        loader.contentLoaderInfo.addEventListener(Event.COMPLETE, onLoaderComplete);

        loader.load(new URLRequest(video_url));
        //loader.load(urlReq, context);


        function onLoaderInit(event:Event):void {
            addChild(loader);

            loader.content.addEventListener("onReady", onPlayerReady);
            loader.content.addEventListener("onError", onPlayerError);
            loader.content.addEventListener("onStateChange", onPlayerStateChange);
            loader.content.addEventListener("onPlaybackQualityStateChange", onPlayerlaybackQualityStateChange);
        }

        function onLoaderComplete(event:Event):void {
            var ldrInfo:LoaderInfo = event.target as LoaderInfo;
            if (ldrInfo)
            {

                trace(ldrInfo.width);
                trace(ldrInfo.height);
            }
        }




        function onPlayerReady(event:Event):void {
            trace("player ready: ", Object(event).data);

            player = loader.content;
            player.setSize(video_width, video_height);

        }

        function onPlayerError(event:Event):void {
            trace("player error: ", Object(event).data);
        }

        function onPlayerStateChange(event:Event):void {
            /*
            -1 (unstarted)
            0 (ended)
            1 (playing)
            2 (paused)
            3 (buffering)
            5 (video cued).*/

            if (Object(event).data == 1) {
                trace("playing");
            }
        }

        function onPlayerlaybackQualityStateChange(event:Event):void {
            trace("player quality: ", Object(event).data);
        }

    }
}
}
