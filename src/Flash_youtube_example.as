package {

import flash.display.Loader;
import flash.display.Sprite;
import flash.events.Event;
import flash.net.URLRequest;
import flash.system.Security;

public class Flash_youtube_example extends Sprite {
    public function Flash_youtube_example() {

        Security.allowDomain("www.youtube.com");

        var player:Object;

        var loader:Loader = new Loader();

        loader.contentLoaderInfo.addEventListener(Event.INIT, onLoaderInit);
        loader.load(new URLRequest("http://www.youtube.com/v/FYaHw7402OQ?version=3"));


        function onLoaderInit(event:Event):void {
            addChild(loader);

            loader.content.addEventListener("onReady", onPlayerReady);
            loader.content.addEventListener("onError", onPlayerError);
            loader.content.addEventListener("onStateChange", onPlayerStateChange);
            loader.content.addEventListener("onPlaybackQualityStateChange", onPlayerlaybackQualityStateChange);
        }


        function onPlayerReady(event:Event):void {
            trace("player ready: ", Object(event).data);

            player = loader.content;
            //player.setSize(640, 480);
        }

        function onPlayerError(event:Event):void {
            trace("player error: ", Object(event).data);
        }

        function onPlayerStateChange(event:Event):void {
            if (Object(event).data == 1) {
                //press play button
            }
        }

        function onPlayerlaybackQualityStateChange(event:Event):void {
            trace("player quality: ", Object(event).data);
        }

    }
}
}
