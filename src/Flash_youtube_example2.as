package {

import flash.display.Loader;
import flash.display.MovieClip;
import flash.display.Sprite;
import flash.events.Event;
import flash.events.HTTPStatusEvent;
import flash.events.IEventDispatcher;
import flash.events.IOErrorEvent;
import flash.events.MouseEvent;
import flash.events.ProgressEvent;
import flash.net.URLRequest;
import flash.system.ApplicationDomain;
import flash.system.LoaderContext;
import flash.system.Security;
import flash.system.SecurityDomain;
import flash.text.TextField;

public class Flash_youtube_example2 extends Sprite {
    public function Flash_youtube_example2() {

        Security.allowDomain("*.youtube.com");
        Security.allowDomain("*.googlesyndication.com");




        var context:LoaderContext = new LoaderContext();
        context.checkPolicyFile = false;
        //context.securityDomain = SecurityDomain.currentDomain;
        context.applicationDomain = ApplicationDomain.currentDomain;
        var urlReq:URLRequest = new URLRequest("http://www.youtube.com/v/mKPWna3jGjQ");
        var ldr:Loader = new Loader();
        ldr.load(urlReq, context);

        trace("1232");

        addChild(ldr);

        //var request:URLRequest = new URLRequest("http://www.youtube.com/v/mKPWna3jGjQ");
        //var loader:Loader = new Loader();
        //LoaderContext(false, new ApplicationDomain(), null);
//        loader.mouseChildren = false;
//        loader.addEventListener(MouseEvent.CLICK, clickHandler);

        //loader.addEventListener(MouseEvent.CLICK, clickHandler, true);

//        configureListeners(loader.contentLoaderInfo);
//        loader.addEventListener(MouseEvent.CLICK, clickHandler);

        //loader.load(request);
        //addChild(loader);

//
//        var newMC:MovieClip = new MovieClip();
//
//        newMC.
//        addChild(newMC);

//        var sprite:Sprite = new Sprite();
//        var clip:MovieClip = new MovieClip();
//
//        clip.loadMovie("http://www.youtube.com/v/-jW1vWgogLQ");
//        parent.addChild(clip);

        //this.createEmptyMovieClip("video_mc", 10000);
        //this.video_mc.loadMovie("http://www.youtube.com/v/-jW1vWgogLQ");

//        Security.allowDomain("www.youtube.com");
//
//        var my_player:Object;
//
//        var my_loader:Loader = new Loader();
//        my_loader.load(new URLRequest("http://www.youtube.com/apiplayer?version=3"));
//        my_loader.contentLoaderInfo.addEventListener(Event.INIT, onLoaderInit);
//
//        function onLoaderInit(e:Event):void{
//            addChild(my_loader);
//            my_player = my_loader.content;
//            my_player.addEventListener("onReady", onPlayerReady);
//        }
//
//        function onPlayerReady(e:Event):void{
//            my_player.setSize(640,360);
//            //my_player.cueVideoById("M7lc1UVf-VE",0);
//
//
//            my_player.cueVideoByUrl('https://www.youtube.com/embed/M7lc1UVf-VE');
//
//        }
    }


    private function configureListeners(dispatcher:IEventDispatcher):void {
        dispatcher.addEventListener(Event.COMPLETE, completeHandler);
        dispatcher.addEventListener(HTTPStatusEvent.HTTP_STATUS, httpStatusHandler);
        dispatcher.addEventListener(Event.INIT, initHandler);
        dispatcher.addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
        dispatcher.addEventListener(Event.OPEN, openHandler);
        dispatcher.addEventListener(ProgressEvent.PROGRESS, progressHandler);
        dispatcher.addEventListener(Event.UNLOAD, unLoadHandler);
    }

    private function completeHandler(event:Event):void {
        trace("completeHandler: " + event);
    }

    private function httpStatusHandler(event:HTTPStatusEvent):void {
        trace("httpStatusHandler: " + event);
    }

    private function initHandler(event:Event):void {
        trace("initHandler: " + event);
    }

    private function ioErrorHandler(event:IOErrorEvent):void {
        trace("ioErrorHandler: " + event);
    }

    private function openHandler(event:Event):void {
        trace("openHandler: " + event);
    }

    private function progressHandler(event:ProgressEvent):void {
        trace("progressHandler: bytesLoaded=" + event.bytesLoaded + " bytesTotal=" + event.bytesTotal);
    }

    private function unLoadHandler(event:Event):void {
        trace("unLoadHandler: " + event);
    }

    private function clickHandler(event:MouseEvent):void {
        trace("clickHandler: " + event);
    }


}
}